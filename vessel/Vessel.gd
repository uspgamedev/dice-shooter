extends KinematicBody

#signal debug(data)
signal shoot(projectile, from, to)

export var bullet_scn: PackedScene

export var ACCEL := 10
export var DAMP := 0.9
export var MAXSPD := 2.0
export var TURNACC := 5
export var TURNSPD := PI / 3

onready var motion := Vector3.ZERO
onready var turn := 0.0
onready var pitch := 0.0

func _physics_process(delta: float):
	process_movement(delta)
	process_shooting()

func _process(_delta):
	$Camera.rotation.z = - PI / 16 * turn
	$Camera.rotation.x = - PI / 24 * pitch

func process_movement(delta: float):
	var frontal := Input.get_action_strength("move_down") \
				 - Input.get_action_strength("move_up")
	var lateral := Input.get_action_strength("move_right") \
				 - Input.get_action_strength("move_left")
	var dir := frontal * Vector3.DOWN + lateral * Vector3.RIGHT
	var accel: Vector3 = dir * ACCEL * delta
	motion += accel * delta
	motion *= DAMP
	if motion.length_squared() > MAXSPD * MAXSPD:
		motion = motion.normalized() * MAXSPD
	if motion.length_squared() < 0.01:
		motion = Vector3.ZERO
	pitch = move_toward(pitch, sign(frontal), abs(frontal) * delta * TURNACC)
	pitch *= 0.9
	turn = move_toward(turn, sign(lateral), abs(lateral) * delta * TURNACC)
	turn *= 0.9
	return move_and_collide(motion * delta)

func process_shooting():
	if Input.is_action_pressed("shoot"):
		if $ShootingCooldown.is_stopped():
			_shoot()
			$ShootingCooldown.start()
	else:
		$ShootingCooldown.stop()

func _shoot():
	if not Input.is_action_pressed("shoot"):
		return
	var bullet_l := bullet_scn.instance() as Area
	var bullet_r := bullet_scn.instance() as Area
	var mouse_pos := get_viewport().get_mouse_position()
	var normal := $Camera.project_ray_normal(mouse_pos) as Vector3
	emit_signal(
		"shoot",
		bullet_l,
		$Camera/LeftBlaster/ShootOrigin.global_transform.origin,
		normal
	)
	emit_signal(
		"shoot",
		bullet_r,
		$Camera/RightBlaster/ShootOrigin.global_transform.origin,
		normal
	)
