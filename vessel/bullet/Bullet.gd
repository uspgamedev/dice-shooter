extends Area

export var speed := 20

func _physics_process(delta):
	global_translate(-transform.basis.z * speed * delta)


func _on_body_entered(body: PhysicsBody):
	if body is Die:
		body.take_damage()
	queue_free()
