class_name Die extends KinematicBody


export var hp := 50
export var spd := 5

onready var axis := Vector3.FORWARD
onready var rotspd := rand_range(PI / 4, PI / 2)

func _ready():
	var phi := rand_range(-PI, PI)
	var theta := rand_range(-PI/2, PI/2)
	axis = axis.rotated(Vector3.UP, phi)
	var ortho := Vector3(-axis.z, axis.y, axis.x)
	axis = axis.rotated(ortho, theta)

func _physics_process(delta):
	rotate(axis, rotspd * delta)
	return move_and_collide(Vector3.BACK * spd * delta)

func take_damage():
	hp -= 1
	if hp <= 0:
		queue_free()
