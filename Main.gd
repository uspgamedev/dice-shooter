extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func _debug(data: Dictionary):
	var text := ""
	for key in data:
		text += "%s: %s\n" % [key, data[key]]
	$Label.text = text

func _spawn(obj: Spatial, from: Vector3, to: Vector3):
	$Arena.add_child(obj)
	
	obj.translation = from
	obj.look_at(from + to, Vector3.UP)
